﻿using System;
using TMI.Core;
using UnityEngine;

public class EnemyRotateSubstate : BaseEnemySubstate {

    private readonly TimeSpan testPathValidityTimeDelay;
    private TimeSpan currentPathValidityTestTimeSpan = TimeSpan.Zero;
    private bool previousCanMoveForwardTestResult = true;

    private const float rotateDegrees = 90f;

    private readonly Vector3 globalAxis;
    private readonly float rotationPerSecond;
    private readonly Vector3 rotateAroundPoint;
    private readonly Vector3 localAxis;


    private float currentRotation = 0f;
    private float previousHeight;
    private Quaternion endRotation;


    private Vector3 startingPosition;
    private Vector3 direction;

    public EnemyRotateSubstate(IInitializer initializer,
        IEnemyBehaviour proxy,
        TimeSpan stepDuration,
        Vector3 globalAxis,
        Vector3 rotateAroundPoint,
        Vector3 localAxis) : base(initializer, proxy) {

        this.globalAxis = globalAxis;
        this.rotationPerSecond = rotateDegrees / (float)stepDuration.TotalSeconds;
        this.rotateAroundPoint = rotateAroundPoint;
        this.localAxis = localAxis;

        this.testPathValidityTimeDelay = TimeSpan.FromSeconds(stepDuration.TotalSeconds / 4f);
    }

    public override void Enter() {
        base.Enter();

        startingPosition = proxy.transform.position;
        startingPosition.y = 0.1f;

        Vector3 endPosition = rotateAroundPoint;
        endPosition.y = 0.1f;

        direction = (endPosition - startingPosition).normalized;



        Quaternion rotation = proxy.transform.rotation;
        endRotation = Quaternion.Euler(rotation.eulerAngles + globalAxis * rotateDegrees);

        //save height
        previousHeight = proxy.transform.position.y;
    }

    public override void Update() {
        base.Update();

        //check if current forward rotation path is free
        if(canMoveForward) {
            if(currentRotation < rotateDegrees) {
                float rotateByDegrees = rotationPerSecond * Time.deltaTime;
                proxy.transform.RotateAround(rotateAroundPoint, localAxis, rotateByDegrees);
                currentRotation += rotateByDegrees;
            } else {
                nextState = proxy.CreateDecisionState();
            }
        }

    }

    private bool canMoveForward {
        get {
            if(timeInState > currentPathValidityTestTimeSpan) {
                currentPathValidityTestTimeSpan = timeInState + testPathValidityTimeDelay;

                const float raycastLengthMultiplier = 4f;
                if(Physics.Raycast(startingPosition, direction, out RaycastHit hitInfo, proxy.modelScale * raycastLengthMultiplier, 1 << LayerMask.NameToLayer("Enemy"))) {

                    //if we hit another cube, then stop the movement for a bit
                    if(hitInfo.collider != proxy.mainCollider) {
                        previousCanMoveForwardTestResult = false;
                    }
                } else {
                    previousCanMoveForwardTestResult = true;
                }
            }

            return previousCanMoveForwardTestResult;
        }
    }

    public override void Exit() {
        proxy.transform.rotation = endRotation;

        //reset height
        Vector3 currentPosition = proxy.transform.position;
        currentPosition.y = previousHeight;
        proxy.transform.position = currentPosition;
        base.Exit();
    }
}
