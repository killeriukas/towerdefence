﻿using TMI.Core;

public class StandardCubeDecisionSubstate : BaseEnemyDecisionSubstate {

    public StandardCubeDecisionSubstate(IInitializer initializer, IEnemyBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();

        nextState = new EnemyRecalculateForwardSubstate(initializer, proxy);
    }

}
