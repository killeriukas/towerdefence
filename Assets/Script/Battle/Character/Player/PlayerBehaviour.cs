﻿using TMI.Core;
using TMI.Mathematics;
using TMI.Notification;
using TMI.State;
using UnityEngine;

public class PlayerBehaviour : BaseNotificationBehaviour, IPlayerBehaviour, IUpdatable {

    [SerializeField]
    private WeaponBehaviour[] availableWeapons;

    [SerializeField]
    private PlayerData playerData;

    [SerializeField]
    private Collider _collider;

    [SerializeField]
    private BaseCameraBehaviour[] availableCameras;

    private int currentWeaponIndex = 0;
    private WeaponBehaviour _currentWeapon;
    public IWeaponBehaviour currentWeapon => _currentWeapon;

    private int currentHealth;

    private bool isAlive => currentHealth > 0;

    private IExecutionManager executionManager;
    private IStateMachine stateMachine;

    private BaseCameraBehaviour _currentCamera;

    public Camera currentCamera => _currentCamera.mainCamera;
    public Collider mainCollider => _collider;
    public IUnit hpUnit => new UnitDouble(currentHealth, playerData.health);
    public string healthDescription => "HP " + hpUnit.description;

    protected override void Awake() {
        base.Awake();

        foreach(WeaponBehaviour wb in availableWeapons) {
            wb.gameObject.SetActive(false);
        }

        foreach(BaseCameraBehaviour cb in availableCameras) {
            cb.gameObject.SetActive(false);
        }

        _currentCamera = availableCameras[0];
        _currentCamera.gameObject.SetActive(true);

        currentHealth = playerData.health;
        _collider.enabled = false;
    }

    public void Damage(int damage) {
        currentHealth -= damage;

        if(currentHealth < 0) {
            currentHealth = 0;
        }

        Trigger(new PlayerDamagedNotification());

        if(!isAlive) {
            Trigger(new PlayerDiedNotification());
        }
    }

    public override void Setup(IInitializer initializer) {
        base.Setup(initializer);
        executionManager = initializer.GetManager<IExecutionManager>();

        foreach(WeaponBehaviour wb in availableWeapons) {
            wb.Setup(initializer);
        }
    }

    public void Initialize() {
        _currentWeapon = availableWeapons[currentWeaponIndex];
        _currentWeapon.gameObject.SetActive(true);

        stateMachine = StateMachine.Create(new PlayerSpawnState(initializer, this));

        executionManager.Register(this, OnUpdate);
    }

    private void ChangeWeapon(int index) {
        _currentWeapon.EndShooting();
        _currentWeapon.gameObject.SetActive(false);

        currentWeaponIndex = index;

        _currentWeapon = availableWeapons[index];
        _currentWeapon.gameObject.SetActive(true);
    }

    private void ChangeCamera(int index) {
        _currentCamera.gameObject.SetActive(false);
        _currentCamera = availableCameras[index];
        _currentCamera.gameObject.SetActive(true);
        Trigger(new CameraChangedNotification(index == 0));
    }

    private ExecutionManager.Result OnUpdate() {
        stateMachine.Update();

        if(Input.GetKeyDown(KeyCode.Alpha1)) {
            ChangeWeapon(0);
        }

        if(Input.GetKeyDown(KeyCode.Alpha2)) {
            ChangeWeapon(1);
        }

        if(Input.GetKeyDown(KeyCode.Alpha3)) {
            ChangeWeapon(2);
        }

        if(Input.mouseScrollDelta.y > 0) {
            ++currentWeaponIndex;
            currentWeaponIndex %= availableWeapons.Length;
            ChangeWeapon(currentWeaponIndex);
        }

        if(Input.mouseScrollDelta.y < 0) {
            --currentWeaponIndex;
            currentWeaponIndex = currentWeaponIndex < 0 ? availableWeapons.Length - 1 : currentWeaponIndex;
            ChangeWeapon(currentWeaponIndex);
        }

        if(Input.GetKeyDown(KeyCode.Alpha9)) {
            ChangeCamera(0);
        }

        if(Input.GetKeyDown(KeyCode.Alpha0)) {
            ChangeCamera(1);
        }

        //update current camera control here
        _currentCamera.UpdateCameraControl(transform);

        return ExecutionManager.Result.Continue;
    }

    protected override void OnDestroy() {
        executionManager.Unregister(this);
        base.OnDestroy();
    }

}