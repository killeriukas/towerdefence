﻿
public static class SceneConstants {

    public const string battle = "scene_battle";
    public const string metagame = "scene_metagame";
    public const string restart = "scene_restart";

}
