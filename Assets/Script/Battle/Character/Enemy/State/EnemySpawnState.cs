﻿using System;
using TMI.Core;

public class EnemySpawnState : BaseEnemyState {

    private static readonly TimeSpan spawnDuration = TimeSpan.FromSeconds(1);

    public EnemySpawnState(IInitializer initializer, IEnemyBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();
        proxy.mainCollider.enabled = false;
    }

    public override void Update() {
        base.Update();
        
        if(timeInState > spawnDuration) {
            nextState = new EnemyMoveState(initializer, proxy);
        }
    }

    public override void Exit() {
        proxy.mainCollider.enabled = true;
        base.Exit();
    }

}
