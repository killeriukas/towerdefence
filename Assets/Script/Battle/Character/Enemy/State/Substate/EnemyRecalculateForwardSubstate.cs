﻿using TMI.Core;
using UnityEngine;

public class EnemyRecalculateForwardSubstate : BaseEnemyRecalculateMovementSubstate {

    public EnemyRecalculateForwardSubstate(IInitializer initializer, IEnemyBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();

        Vector3 forward = CalculateForward();

        float halfScale = proxy.modelScale / 2;

        Vector3 rotateAroundPoint = proxy.transform.position + forward * halfScale;
        rotateAroundPoint.y -= halfScale;

        Vector3 right = Vector3.Cross(Vector2.up, forward);

        nextState = new EnemyRotateSubstate(initializer, proxy, proxy.stepDuration, Vector3.right, rotateAroundPoint, right);
    }
}
