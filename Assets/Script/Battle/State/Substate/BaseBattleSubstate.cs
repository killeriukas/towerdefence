﻿using TMI.Core;
using TMI.State;

public abstract class BaseBattleSubstate : BaseState {

    protected BaseBattleSubstate(IInitializer initializer) : base(initializer) {
    }

}
