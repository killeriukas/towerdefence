﻿using UnityEngine;

[CreateAssetMenu(fileName = "PlayerData", menuName = "Template/Player Data")]
public class PlayerData : ScriptableObject {

    public int health;

}
