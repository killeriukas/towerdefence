﻿
public interface IWeaponBehaviour {
    void StartShooting();
    void EndShooting();
    void PlayShootVfx();
}
