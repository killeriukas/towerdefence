﻿using TMI.Mathematics;
using TMI.Pattern;
using UnityEngine;

public interface IPlayerBehaviour : IProxy {
    IWeaponBehaviour currentWeapon { get; }
    IUnit hpUnit { get; }
    string healthDescription { get; }
    Collider mainCollider { get; }
}
