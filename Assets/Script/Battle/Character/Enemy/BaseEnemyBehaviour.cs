﻿using System;
using TMI.Core;
using TMI.Helper;
using TMI.Notification;
using TMI.State;
using UnityEngine;

public abstract class BaseEnemyBehaviour : BaseNotificationBehaviour, IEnemyBehaviour, IUpdatable {

    [SerializeField]
    private Renderer modelRenderer;

    [SerializeField]
    private EnemyData enemyData;

    [SerializeField]
    private Collider _collider;

    [SerializeField]
    private bool isBoss;

    private IExecutionManager executionManager;
    private BattleManager battleManager;

    private Material material;
    private int currentHealthAmount;
    private GameObjectPool<BaseEnemyBehaviour> pool;
    private IStateMachine stateMachine;

    private bool isAlive => currentHealthAmount > 0;
    public TimeSpan stepDuration { get; private set; }
    public int damage => enemyData.damage;
    public Collider mainCollider => _collider;
    public float modelScale => modelRenderer.transform.localScale.x;

    public event Action<Collider> onTriggerEnter;

    protected override void Awake() {
        base.Awake();
        material = modelRenderer.material;
        _collider.enabled = false;
    }

    public override void Setup(IInitializer initializer) {
        base.Setup(initializer);
        executionManager = initializer.GetManager<IExecutionManager>();
        battleManager = initializer.GetManager<BattleManager>();
    }

    public void Initialize(GameObjectPool<BaseEnemyBehaviour> pool, Vector3 enemyPosition) {
        this.pool = pool;
        transform.position = enemyPosition;

        float stepsPerSecond = battleManager.gameData.mapSize / (float)enemyData.totalTravelDuration.TotalSeconds;
        stepDuration = TimeSpan.FromSeconds(modelScale / stepsPerSecond);

        transform.rotation = CalculateStartingRotation(transform.position, battleManager.playerPosition);

        currentHealthAmount = enemyData.hp;

        Color color = material.color;
        color.a = 1f;
        material.color = color;

        stateMachine = StateMachine.Create(new EnemySpawnState(initializer, this));

        executionManager.Register(this, OnUpdate);
    }

    private static Quaternion CalculateStartingRotation(Vector3 myPosition, Vector3 targetPosition) {
        targetPosition.y = 0f;
        myPosition.y = 0f;

        Vector3 direction = (targetPosition - myPosition).normalized;
        return Quaternion.LookRotation(direction, Vector3.up);
    }

    private void OnTriggerEnter(Collider other) {
        onTriggerEnter?.Invoke(other);
    }

    private ExecutionManager.Result OnUpdate() {
        stateMachine.Update();
        return ExecutionManager.Result.Continue;
    }

    public void Damage(int damage) {
        currentHealthAmount -= damage;

        if(currentHealthAmount < 0) {
            currentHealthAmount = 0;
        }

        Color color = material.color;
        color.a = currentHealthAmount / (float)enemyData.hp;
        material.color = color;

        if(isAlive) {
            VfxVisualAccessor vfxHitSplash = battleManager.vfxEnemySplashPool.Get();
            vfxHitSplash.transform.position = transform.position;
            vfxHitSplash.Initialize(battleManager.vfxEnemySplashPool);
        } else {
            Trigger(new EnemyDiedNotification(GetInstanceID(), isBoss));
        }
    }

    public abstract BaseEnemyDecisionSubstate CreateDecisionState();

    public void TurnOff() {
        executionManager.Unregister(this);
        pool.Release(this);
        GeneralHelper.DisposeAndMakeDefault(ref stateMachine);
    }

    protected override void OnDestroy() {
        if(stateMachine != null) {
            executionManager.Unregister(this);
        }

        GeneralHelper.DisposeAndMakeDefault(ref stateMachine);
        GameObject.Destroy(material);
        base.OnDestroy();
    }


}