﻿public interface IZigZagCubeBehaviour : IEnemyBehaviour {
    ZigZagCubeSettings settings { get; }
}
