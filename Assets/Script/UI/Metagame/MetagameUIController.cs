﻿using System;
using TMI.Core;
using TMI.SceneManagement;
using TMI.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class MetagameUIController : BaseUIController {

    [SerializeField]
    private UIButton startGameButton;

    [SerializeField]
    private UIButton quitGameButton;

    private SceneManager sceneManager;

    public override void Setup(IInitializer initializer) {
        base.Setup(initializer);
        sceneManager = initializer.GetManager<SceneManager>();

        startGameButton.onButtonClick += OnStartGameClicked;
        quitGameButton.onButtonClick += OnQuitGameClicked;
    }

    private void OnQuitGameClicked(PointerEventData data) {
        Application.Quit();
    }

    private void OnStartGameClicked(PointerEventData data) {
        LoadingScreenUIController loadingScreen = uiManager.LoadUI<LoadingScreenUIController>();
        loadingScreen.Show();

        sceneManager.LoadScene(SceneConstants.battle);

        Hide();
    }

    protected override void OnDestroy() {
        startGameButton.onButtonClick -= OnStartGameClicked;
        quitGameButton.onButtonClick -= OnQuitGameClicked;
        base.OnDestroy();
    }

}