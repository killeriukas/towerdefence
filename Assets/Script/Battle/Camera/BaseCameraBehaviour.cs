﻿using TMI.Core;
using UnityEngine;

public abstract class BaseCameraBehaviour : UnityBehaviour {

    [SerializeField]
    private Camera _camera;

    [SerializeField]
    protected float xSensitivity;

    [SerializeField]
    protected float ySensitivity;

    public Camera mainCamera => _camera;

    public abstract void UpdateCameraControl(Transform root);

}