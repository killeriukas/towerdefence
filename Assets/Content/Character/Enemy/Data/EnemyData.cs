﻿using System;
using TMI.Core;
using UnityEngine;

[CreateAssetMenu(fileName = "EnemyData", menuName = "Template/Enemy Data")]
public class EnemyData : ScriptableObject {

    [SerializeField]
    private float _totalTravelDurationInSeconds;

    public TimeSpan totalTravelDuration => TimeSpan.FromSeconds(_totalTravelDurationInSeconds);

    public int damage;

    public int hp;


    //private enum Type {
    //    Unknown,
    //    MachineGun,
    //    Shotgun,
    //    Sniper
    //}

    //[SerializeField]
    //private Type weaponType = Type.Unknown;

    //[SerializeField]
    //private float _delayBetweenBulletsInMS;

    //public TimeSpan delayBetweenBullets => TimeSpan.FromMilliseconds(_delayBetweenBulletsInMS);

    //public IWeapon CreateWeapon(IInitializer initializer, IWeaponBehaviour weaponBehaviour) {
    //    switch(weaponType) {
    //        case Type.MachineGun:
    //            return new MachineGunWeapon(initializer, weaponBehaviour, this);
    //    }
    //    throw new ArgumentException("Weapon type is not known. Please select a weapon type!");
    //}

    //public int bulletDamage;

    //public ParticleSystem vfxMuzzleFlashPrefab;

    //public float dispersionRate;

}
