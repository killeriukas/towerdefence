﻿using TMI.Core;
using UnityEngine;

public class EnemyRecalculateLeftSubstate : BaseEnemyRecalculateMovementSubstate {

    public EnemyRecalculateLeftSubstate(IInitializer initializer, IEnemyBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();

        Vector3 forward = CalculateForward();

        float halfScale = proxy.modelScale / 2;

        Vector3 right = Vector3.Cross(Vector2.up, forward);

        Vector3 rotateAroundPoint = proxy.transform.position - right * halfScale;
        rotateAroundPoint.y -= halfScale;

        nextState = new EnemyRotateSubstate(initializer, proxy, proxy.stepDuration, Vector3.right, rotateAroundPoint, forward);
    }
}
