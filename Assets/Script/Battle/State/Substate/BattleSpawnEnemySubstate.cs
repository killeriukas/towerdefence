﻿using TMI.Core;

public class BattleSpawnEnemySubstate : BaseBattleSubstate {

    private BattleManager battleManager;
    private SpawnManager spawnManager;

    public BattleSpawnEnemySubstate(IInitializer initializer) : base(initializer) {
        this.spawnManager = initializer.GetManager<SpawnManager>();
        this.battleManager = initializer.GetManager<BattleManager>();
    }


    public override void Enter() {
        base.Enter();

        if(spawnManager.canSpawnAnotherCube) {
            spawnManager.SpawnRandomEnemy(battleManager.gameData.mapSize);
        }

        nextState = new BattlePendingSubstate(initializer);
    }
}
