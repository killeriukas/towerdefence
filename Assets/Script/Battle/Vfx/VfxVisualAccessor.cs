﻿using TMI.Core;

public class VfxVisualAccessor : UnityBehaviour {

    private GameObjectPool<VfxVisualAccessor> pool;

    public void Initialize(GameObjectPool<VfxVisualAccessor> pool) {
        this.pool = pool;
    }

    private void OnParticleSystemStopped() {
        pool.Release(this);
    }

}
