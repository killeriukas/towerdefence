﻿using System;
using TMI.Core;
using TMI.Helper;
using TMI.State;
using UnityEngine;

public class BattleManager : BaseNotificationManager {

    private PlayerBehaviour _playerBehaviour;
    public IPlayerBehaviour playerBehaviour => _playerBehaviour;

    public Camera currentCamera => _playerBehaviour.currentCamera;

    public GameObjectPool<VfxVisualAccessor> vfxEnemySplashPool { get; private set; }

    public Vector3 playerPosition => _playerBehaviour.transform.position;

    private IStateMachine stateMachine;

    public GameData gameData { get; private set; }

    protected override ExecutionManager.Result OnUpdate() {
        stateMachine.Update();
        return ExecutionManager.Result.Continue;
    }

    public void Initialize(GameObject playerPrefab, GameObject vfxEnemySplash, SpawnAccessor spawnAccessor, GameData gameData) {
        this.gameData = gameData;

        _playerBehaviour = HierarchyHelper.InstantiateAndSetupBehaviour<PlayerBehaviour>(initializer, playerPrefab);
        _playerBehaviour.transform.SetParent(spawnAccessor.playerSpawnTransform, false);
        _playerBehaviour.Initialize();


        vfxEnemySplashPool = new GameObjectPool<VfxVisualAccessor>(initializer, vfxEnemySplash, 5);

        stateMachine = StateMachine.Create(new BattleCountdownState(initializer));

        RegisterUpdate();
    }

    protected override void OnDestroy() {
        UnregisterUpdate();
        vfxEnemySplashPool?.Dispose();
        vfxEnemySplashPool = null;
        base.OnDestroy();
    }
}