﻿using System;
using TMI.Core;
using UnityEngine;

public class EnemyAttackState : BaseEnemyState {

    private PlayerBehaviour playerBehaviour;

    public EnemyAttackState(IInitializer initializer, IEnemyBehaviour proxy, PlayerBehaviour playerBehaviour) : base(initializer, proxy) {
        this.playerBehaviour = playerBehaviour;
    }

    public override void Enter() {
        base.Enter();

        playerBehaviour.Damage(proxy.damage);

        nextState = new EnemyDeathState(initializer, proxy);
    }

}
