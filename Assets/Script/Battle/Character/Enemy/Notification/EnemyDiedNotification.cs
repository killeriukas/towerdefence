﻿using TMI.Notification;

public class EnemyDiedNotification : INotification {

    public readonly int instanceId;
    public readonly bool isBoss;

    public EnemyDiedNotification(int instanceId, bool isBoss) {
        this.instanceId = instanceId;
        this.isBoss = isBoss;
    }

}
