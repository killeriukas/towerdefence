﻿using System;
using TMI.Core;
using UnityEngine;

public class WeaponBehaviour : UnityBehaviour, IWeaponBehaviour, IUpdatable {

    private enum Type {
        Unknown,
        MachineGun,
        Shotgun,
        Sniper
    }

    [SerializeField]
    private WeaponData weaponData;

    [SerializeField]
    private Transform muzzleFlashTransform;

    private IWeapon weapon;

    private IExecutionManager executionManager;
    private BattleManager battleManager;

    private ParticleSystem vfxMuzzleFlash;

    public void PlayShootVfx() {
        vfxMuzzleFlash.Play();
    }

    protected override void Awake() {
        base.Awake();
        vfxMuzzleFlash = GameObject.Instantiate<ParticleSystem>(weaponData.vfxMuzzleFlashPrefab, muzzleFlashTransform, false);
    }

    public override void Setup(IInitializer initializer) {
        base.Setup(initializer);
        executionManager = initializer.GetManager<IExecutionManager>();
        battleManager = initializer.GetManager<BattleManager>();

        weapon = weaponData.CreateWeapon(initializer, this);

        executionManager.Register(this, OnUpdate);
    }

    private ExecutionManager.Result OnUpdate() {
        weapon.Update();
        return ExecutionManager.Result.Continue;
    }

    public void StartShooting() {
        weapon.StartShooting();
    }

    public void EndShooting() {
        weapon.EndShooting();
    }

    protected override void OnDestroy() {
        executionManager.Unregister(this);
        GameObject.Destroy(vfxMuzzleFlash.gameObject);
        base.OnDestroy();
    }

}
