﻿using TMI.Core;
using TMI.State;

public abstract class BaseBattleState : BaseState {

    protected BaseBattleState(IInitializer initializer) : base(initializer) {
    }

}
