﻿using System;
using TMI.Core;
using TMI.Helper;
using TMI.State;
using UnityEngine;

public class Weapon : IWeapon {

    private IInitializer initializer;
    private IStateMachine stateMachine;

    private readonly IWeaponBehaviour weaponBehaviour;
    private readonly WeaponData weaponData;
    private BattleManager battleManager;

    private readonly Vector2 screenCenterCoordinates;
    public bool isShooting { get; private set; }

    public TimeSpan shotDelay => weaponData.delayBetweenBullets;

    public Weapon(IInitializer initializer, IWeaponBehaviour weaponBehaviour, WeaponData weaponData) {
        this.initializer = initializer;
        this.weaponBehaviour = weaponBehaviour;
        this.weaponData = weaponData;

        this.battleManager = initializer.GetManager<BattleManager>();
        this.screenCenterCoordinates = new Vector2(Screen.width / 2, Screen.height / 2);

        stateMachine = StateMachine.Create(new WeaponShootState(initializer, this));
    }

    public void StartShooting() {
        isShooting = true;
    }

    public void Update() {
        stateMachine.Update();
    }

    protected bool CastRay(out RaycastHit hitInfo) {
        Camera currentCamera = battleManager.currentCamera;

        Vector2 randomTargetPoint = UnityEngine.Random.insideUnitCircle * weaponData.dispersionRate;
        Ray ray = currentCamera.ScreenPointToRay(screenCenterCoordinates + randomTargetPoint);

        //DEBUG - delete later
      //  Debug.DrawLine(ray.origin, ray.origin + ray.direction * 40f, Color.red, 5f);

        return Physics.Raycast(ray, out hitInfo, Mathf.Infinity);
    }

    public void EndShooting() {
        isShooting = false;
    }

    public void Shoot() {
        for(int i = 0; i < weaponData.oneShotBulletAmount; ++i) {
            RaycastHit hitInfo;
            if(CastRay(out hitInfo)) {
                Rigidbody colliderRigidBody = hitInfo.rigidbody;
                if(colliderRigidBody != null) {
                    BaseEnemyBehaviour enemyBehaviour = colliderRigidBody.GetComponent<BaseEnemyBehaviour>();
                    enemyBehaviour?.Damage(weaponData.bulletDamage);
                }
            }
        }

        weaponBehaviour.PlayShootVfx();
    }

    public void Dispose() {
        isShooting = false;
        GeneralHelper.DisposeAndMakeDefault(ref stateMachine);
    }

}
