﻿using TMI.Core;
using TMI.State;

public abstract class BaseEnemySubstate : BaseStateWithProxy<IEnemyBehaviour> {

    protected BaseEnemySubstate(IInitializer initializer, IEnemyBehaviour proxy) : base(initializer, proxy) {
    }

}
