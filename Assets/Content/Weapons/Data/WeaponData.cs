﻿using System;
using TMI.Core;
using UnityEngine;

[CreateAssetMenu(fileName="WeaponData", menuName="Template/Weapon Data")]
public class WeaponData : ScriptableObject {

    private enum Type {
        Unknown,
        MachineGun,
        Shotgun,
        Sniper
    }

    [SerializeField]
    private Type weaponType = Type.Unknown;

    [SerializeField]
    private float _delayBetweenBulletsInMS;

    public TimeSpan delayBetweenBullets => TimeSpan.FromMilliseconds(_delayBetweenBulletsInMS);

    public IWeapon CreateWeapon(IInitializer initializer, IWeaponBehaviour weaponBehaviour) {
        switch(weaponType) {
            case Type.MachineGun:
            case Type.Sniper:
            case Type.Shotgun:
                return new Weapon(initializer, weaponBehaviour, this);
        }
        throw new ArgumentException("Weapon type is not known. Please select a weapon type!");
    }

    public int bulletDamage;

    public ParticleSystem vfxMuzzleFlashPrefab;

    public float dispersionRate;

    public int oneShotBulletAmount;

}
