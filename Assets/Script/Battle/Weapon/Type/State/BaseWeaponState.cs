﻿using TMI.Core;
using TMI.State;

public abstract class BaseWeaponState : BaseStateWithProxy<IWeapon> {

    protected BaseWeaponState(IInitializer initializer, IWeapon proxy) : base(initializer, proxy) {
    }

}
