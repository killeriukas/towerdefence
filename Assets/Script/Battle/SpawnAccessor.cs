﻿using UnityEngine;

public class SpawnAccessor : MonoBehaviour {

    public Transform playerSpawnTransform;

    public GameObject simpleCubeEnemyPrefab;
    public GameObject jumpingCubeEnemyPrefab;
    public GameObject zigZagCubeEnemyPrefab;
    public GameObject titanCubeEnemyPrefab;

}
