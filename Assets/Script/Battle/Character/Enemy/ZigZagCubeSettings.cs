﻿
public class ZigZagCubeSettings {

    public enum Rotation {
        Forward,
        Left,
        Right
    }

    public const int moveLeft = 2;
    public const int moveRight = 2;
    public const int moveForward = 4;

    public Rotation currentRotationDirection = Rotation.Forward;

    public int currentRotation = 0;

    public Rotation nextRotationDirection = Rotation.Right;

    public Rotation inverseNextRotation => nextRotationDirection == Rotation.Left ? Rotation.Right : Rotation.Left;
}
