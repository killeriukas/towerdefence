﻿using System;
using TMI.Core;
using TMI.Helper;
using TMI.State;
using UnityEngine;

public class EnemyMoveState : BaseEnemyState {

    private IStateMachine stateMachine;

    public EnemyMoveState(IInitializer initializer, IEnemyBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();

        proxy.onTriggerEnter += OnTriggerEntered;

        BaseEnemySubstate enemySubstate = proxy.CreateDecisionState();
        stateMachine = StateMachine.Create(enemySubstate);

        Listen<EnemyDiedNotification>(this, OnEnemyDied);
    }

    private void OnEnemyDied(EnemyDiedNotification notification) {
        if(notification.instanceId == proxy.GetInstanceID()) {
            nextState = new EnemyDeathState(initializer, proxy);
        }
    }

    private void OnTriggerEntered(Collider collider) {
        Rigidbody rigidBody = collider.attachedRigidbody;
        if(rigidBody != null) {
            PlayerBehaviour playerBehaviour = rigidBody.gameObject.GetComponent<PlayerBehaviour>();
            if(playerBehaviour != null) {
                proxy.mainCollider.enabled = false;
                nextState = new EnemyAttackState(initializer, proxy, playerBehaviour);
            }
        }
    }

    public override void Update() {
        base.Update();
        stateMachine.Update();
    }

    public override void Exit() {
        proxy.onTriggerEnter -= OnTriggerEntered;
        GeneralHelper.DisposeAndMakeDefault(ref stateMachine);
        base.Exit();
    }

}
