﻿using TMI.Core;

public class EnemyDeathState : BaseEnemyState {

    public EnemyDeathState(IInitializer initializer, IEnemyBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();
        proxy.TurnOff();
    }

}
