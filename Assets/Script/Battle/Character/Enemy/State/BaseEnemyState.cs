﻿using TMI.Core;
using TMI.State;

public abstract class BaseEnemyState : BaseStateWithProxy<IEnemyBehaviour> {

    protected BaseEnemyState(IInitializer initializer, IEnemyBehaviour proxy) : base(initializer, proxy) {
    }

}
