﻿using TMI.Core;

public class RestartInitializer : BaseMainMiniInitializer {

    private void Start() {
        sceneManager.LoadScene(SceneConstants.metagame);
    }

}