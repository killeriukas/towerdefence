﻿using TMI.Core;
using UnityEngine;

public abstract class BaseEnemyRecalculateMovementSubstate : BaseEnemySubstate {

    private BattleManager battleManager;

    public BaseEnemyRecalculateMovementSubstate(IInitializer initializer, IEnemyBehaviour proxy) : base(initializer, proxy) {
        this.battleManager = initializer.GetManager<BattleManager>();
    }

    protected Vector3 CalculateForward() {
        Vector3 targetPosition = battleManager.playerPosition;
        targetPosition.y = 0f;

        Vector3 enemyPosition = proxy.transform.position;
        enemyPosition.y = 0f;

        Vector3 forward = (targetPosition - enemyPosition).normalized;

        return forward;
    }

}
