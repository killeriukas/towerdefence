﻿using System;
using TMI.Core;
using TMI.UI;
using UnityEngine;
using UnityEngine.EventSystems;

public class BattleUIController : BaseUIController, IUpdatable {

    [SerializeField]
    private UIButton mainMenuButton;

    [SerializeField]
    private UIButton resumeButton;

    [SerializeField]
    private GameObject optionsMenu;

    [SerializeField]
    private UIProgressBarPro playerHpBar;

    [SerializeField]
    private UITextPro gameOverText;

    [SerializeField]
    private GameObject rearMirrorGo;

    private bool isOptionMenuActive = false;

    private BattleManager battleManager;
    private IExecutionManager executionManager;

    private IPlayerBehaviour playerBehaviour => battleManager.playerBehaviour;

    public override void Setup(IInitializer initializer) {
        base.Setup(initializer);
        executionManager = initializer.GetManager<IExecutionManager>();
        battleManager = initializer.GetManager<BattleManager>();

        PauseGame(false);

        resumeButton.onButtonClick += OnResumeGameClicked;
        mainMenuButton.onButtonClick += OnMainMenuClicked;

        gameOverText.text = "";
        executionManager.Register(this, OnUpdate);

        Listen<PlayerSpawnedNotification>(this, OnPlayerSpawned);
        Listen<PlayerDamagedNotification>(this, OnPlayerDamaged);
        Listen<PlayerDiedNotification>(this, OnPlayerDied);
        Listen<EnemyDiedNotification>(this, OnEnemyDied);
        Listen<CameraChangedNotification>(this, OnCameraChanged);
    }

    private void OnMainMenuClicked(PointerEventData data) {
        PauseGame(false);
        Trigger(new GameTerminatedNotification());
    }

    private void OnCameraChanged(CameraChangedNotification notification) {
        rearMirrorGo.SetActive(notification.isCurrentFpsCamera);
    }

    private void OnEnemyDied(EnemyDiedNotification notification) {
        if(notification.isBoss) {
            gameOverText.text = "You Won";
        }
    }

    private void OnPlayerDied(PlayerDiedNotification notification) {
        gameOverText.text = "Enemy Won";
    }

    private void OnPlayerDamaged(PlayerDamagedNotification notification) {
        RefreshPlayerHp();
    }

    private void RefreshPlayerHp() {
        playerHpBar.fillAmount = playerBehaviour.hpUnit.ToFloat();
        playerHpBar.text = playerBehaviour.healthDescription;
    }

    private void OnPlayerSpawned(PlayerSpawnedNotification notification) {
        RefreshPlayerHp();
    }

    private ExecutionManager.Result OnUpdate() {

        if(Input.GetKeyDown(KeyCode.Escape)) {
            PauseGame(!isOptionMenuActive);
        }

        return ExecutionManager.Result.Continue;
    }

    private void PauseGame(bool pause) {
        isOptionMenuActive = pause;

        Cursor.lockState = isOptionMenuActive ? CursorLockMode.None : CursorLockMode.Locked;
        Cursor.visible = isOptionMenuActive;

        Time.timeScale = isOptionMenuActive ? 0f : 1f;
        optionsMenu.SetActive(isOptionMenuActive);
    }

    private void OnResumeGameClicked(PointerEventData data) {
        PauseGame(!isOptionMenuActive);
    }

    protected override void OnDestroy() {
        resumeButton.onButtonClick -= OnResumeGameClicked;
        mainMenuButton.onButtonClick -= OnMainMenuClicked;
        executionManager.Unregister(this);

        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;

        base.OnDestroy();
    }

}