﻿using TMI.Notification;

public class CameraChangedNotification : INotification {

    public readonly bool isCurrentFpsCamera;

    public CameraChangedNotification(bool isCurrentFpsCamera) {
        this.isCurrentFpsCamera = isCurrentFpsCamera;
    }

}
