﻿using System;
using System.Collections.Generic;
using TMI.Core;
using TMI.Helper;
using UnityEngine;

public class GameObjectPool<TObjectType> : IDisposable where TObjectType : UnityBehaviour {

    private UnityEngine.Object prefab;
    private IInitializer initializer;

    private static readonly string poolTypeName = typeof(TObjectType).Name;

    private readonly GameObject holder;

    private readonly List<TObjectType> pool;
    private readonly List<TObjectType> inUse;

    public GameObjectPool(IInitializer initializer, UnityEngine.Object prefab, int poolSize) {
        this.prefab = prefab;
        this.initializer = initializer;
        pool = new List<TObjectType>(poolSize);
        inUse = new List<TObjectType>(poolSize);

        holder = new GameObject("pool_" + poolTypeName);

        for(int i = 0; i < poolSize; ++i) {
            TObjectType initObject = InitializeObject(initializer, prefab);
            pool.Add(initObject);
        }
    }

    private TObjectType InitializeObject(IInitializer initializer, UnityEngine.Object prefab) {
        TObjectType go = HierarchyHelper.InstantiateAndSetupBehaviour<TObjectType>(initializer, prefab);
        go.transform.SetParent(holder.transform, false);
        go.gameObject.SetActive(false);
        return go;
    }

    public TObjectType Get() {

        if(0 == pool.Count) {
            TObjectType initObject = InitializeObject(initializer, prefab);
            pool.Add(initObject);
        }

        TObjectType go = pool[0];
        pool.RemoveAt(0);
        inUse.Add(go);

        go.gameObject.SetActive(true);
        return go;
    }

    public void Release(TObjectType releasedObject) {
        bool wasRemoved = inUse.Remove(releasedObject);
        if(wasRemoved) {
            releasedObject.gameObject.SetActive(false);

            //reset object here
            pool.Add(releasedObject);
        } else {
            throw new ArgumentException("Releasing an object, which isn't from this pool!");
        }
    }

    public void Dispose() {
        foreach(TObjectType go in pool) {
            GameObject.Destroy(go);
        }
        pool.Clear();

        foreach(TObjectType go in inUse) {
            GameObject.Destroy(go);
        }
        inUse.Clear();

        GameObject.Destroy(holder);
    }

}