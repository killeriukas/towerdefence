﻿public class StandardCubeBehaviour : BaseEnemyBehaviour {

    public override BaseEnemyDecisionSubstate CreateDecisionState() {
        return new StandardCubeDecisionSubstate(initializer, this);
    }

}