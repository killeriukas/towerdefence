﻿using TMI.AssetManagement;
using TMI.Core;
using TMI.UI;
using UnityEngine;

public class BattleInitializer : BaseCacheUIMiniInitializer {

    [SerializeField]
    private SpawnAccessor spawnAccessor;

    [SerializeField]
    private GameObject playerPrefab;

    [SerializeField]
    private GameObject vfxEnemySplashPrefab;

    [SerializeField]
    private GameData gameData;

    private BattleManager battleManager;
    private SpawnManager spawnManager;

    protected override ISceneGroup CreateUIScenesCache() {
        ISceneGroup sceneGroup = SceneGroup.Create();
        sceneGroup.Add("ui_scene_battle");
        return sceneGroup;
    }

    protected override void OnUIScenesCached() {
        const int startingCubePoolAmount = 3;
        spawnManager.Initialize(spawnAccessor, startingCubePoolAmount, gameData);
        battleManager.Initialize(playerPrefab, vfxEnemySplashPrefab, spawnAccessor, gameData);

        BattleUIController battleUIController = uiManager.LoadUI<BattleUIController>();
        battleUIController.Show();

        LoadingScreenUIController loadingScreen = uiManager.LoadUI<LoadingScreenUIController>();
        loadingScreen.Hide();
    }

    protected override void RegisterManagers(IAcquirer acquirer) {
        base.RegisterManagers(acquirer);
        battleManager = acquirer.AcquireManager<BattleManager>();
        spawnManager = acquirer.AcquireManager<SpawnManager>();
    }

    protected override void OnDestroy() {
        uiManager.UnloadUIScene("ui_scene_battle");
        base.OnDestroy();
    }

}