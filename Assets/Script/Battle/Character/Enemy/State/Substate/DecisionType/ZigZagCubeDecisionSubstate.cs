﻿using TMI.Core;

public class ZigZagCubeDecisionSubstate : BaseEnemyDecisionSubstate {

    public ZigZagCubeDecisionSubstate(IInitializer initializer, IZigZagCubeBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();

        IZigZagCubeBehaviour zigZagCubeBehaviour = (IZigZagCubeBehaviour)proxy;
        ZigZagCubeSettings zigZagCubeSettings = zigZagCubeBehaviour.settings;

        switch(zigZagCubeSettings.currentRotationDirection) {
            case ZigZagCubeSettings.Rotation.Forward:
                if(zigZagCubeSettings.currentRotation < ZigZagCubeSettings.moveForward) {
                    ++zigZagCubeSettings.currentRotation;
                    nextState = new EnemyRecalculateForwardSubstate(initializer, proxy);
                } else {
                    zigZagCubeSettings.currentRotation = 1;
                    zigZagCubeSettings.currentRotationDirection = zigZagCubeSettings.nextRotationDirection;
                    zigZagCubeSettings.nextRotationDirection = zigZagCubeSettings.inverseNextRotation;
                    nextState = GetRotationState(zigZagCubeSettings.currentRotationDirection);
                }
                break;
            case ZigZagCubeSettings.Rotation.Right:
                if(zigZagCubeSettings.currentRotation < ZigZagCubeSettings.moveRight) {
                    ++zigZagCubeSettings.currentRotation;
                    nextState = new EnemyRecalculateRightSubstate(initializer, proxy);
                } else {
                    zigZagCubeSettings.currentRotation = 1;
                    zigZagCubeSettings.currentRotationDirection = ZigZagCubeSettings.Rotation.Forward;
                    nextState = new EnemyRecalculateForwardSubstate(initializer, proxy);
                }
                break;
            case ZigZagCubeSettings.Rotation.Left:
                if(zigZagCubeSettings.currentRotation < ZigZagCubeSettings.moveLeft) {
                    ++zigZagCubeSettings.currentRotation;
                    nextState = new EnemyRecalculateLeftSubstate(initializer, proxy);
                } else {
                    zigZagCubeSettings.currentRotation = 1;
                    zigZagCubeSettings.currentRotationDirection = ZigZagCubeSettings.Rotation.Forward;
                    nextState = new EnemyRecalculateForwardSubstate(initializer, proxy);
                }
                break;
            default:
                throw new System.ArgumentException("Selected rotation is not defined: " + zigZagCubeSettings.currentRotationDirection);
        }

    }

    private BaseEnemySubstate GetRotationState(ZigZagCubeSettings.Rotation rotation) {
        switch(rotation) {
            case ZigZagCubeSettings.Rotation.Right:
                return new EnemyRecalculateRightSubstate(initializer, proxy);
            case ZigZagCubeSettings.Rotation.Left:
                return new EnemyRecalculateLeftSubstate(initializer, proxy);
            default:
                throw new System.ArgumentException("Invalid argument provided: " + rotation);
        }
    }

}
