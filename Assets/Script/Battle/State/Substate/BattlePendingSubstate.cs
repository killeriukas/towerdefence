﻿using System;
using TMI.Core;

public class BattlePendingSubstate : BaseBattleSubstate {

    private BattleManager battleManager;

    public BattlePendingSubstate(IInitializer initializer) : base(initializer) {
        this.battleManager = initializer.GetManager<BattleManager>();
    }

    public override void Update() {
        base.Update();

        if(timeInState > battleManager.gameData.spawnFrequenceDuration) {
            nextState = new BattleSpawnEnemySubstate(initializer);
        }

    }
}
