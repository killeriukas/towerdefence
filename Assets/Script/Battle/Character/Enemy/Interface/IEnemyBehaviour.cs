﻿using System;
using TMI.Pattern;
using UnityEngine;

public interface IEnemyBehaviour : IProxy {
    Transform transform { get; }
    TimeSpan stepDuration { get; }
    event Action<Collider> onTriggerEnter;
    int damage { get; }

    void TurnOff();
    int GetInstanceID();
    Collider mainCollider { get; }
    float modelScale { get; }

    BaseEnemyDecisionSubstate CreateDecisionState();
}
