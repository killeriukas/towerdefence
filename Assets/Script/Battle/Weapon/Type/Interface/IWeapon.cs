﻿using System;
using TMI.Pattern;

public interface IWeapon : IProxy, IDisposable {
    TimeSpan shotDelay { get; }

    void StartShooting();
    void EndShooting();
    void Update();
    void Shoot();
    bool isShooting { get; }
}
