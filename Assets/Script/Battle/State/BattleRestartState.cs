﻿using TMI.Core;
using TMI.SceneManagement;
using TMI.UI;

public class BattleRestartState : BaseBattleState {

    private SceneManager sceneManager;
    private IUIManager uiManager;

    public BattleRestartState(IInitializer initializer) : base(initializer) {
        this.sceneManager = initializer.GetManager<SceneManager>();
        this.uiManager = initializer.GetManager<IUIManager>();
    }

    public override void Enter() {
        base.Enter();
        LoadingScreenUIController loadingScreenUIController = uiManager.LoadUI<LoadingScreenUIController>();
        loadingScreenUIController.Show();

        sceneManager.LoadScene(SceneConstants.restart);
    }

}
