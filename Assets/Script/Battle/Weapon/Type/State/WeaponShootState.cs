﻿using TMI.Core;

public class WeaponShootState : BaseWeaponState {

    public WeaponShootState(IInitializer initializer, IWeapon proxy) : base(initializer, proxy) {
    }

    public override void Update() {
        base.Update();

        if(proxy.isShooting) {
            proxy.Shoot();
            nextState = new WeaponShootDelayState(initializer, proxy);
        }
    }

}
