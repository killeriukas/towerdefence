﻿using TMI.Core;

public class JumpingCubeDecisionSubstate : BaseEnemyDecisionSubstate {

    private float jumpChance = 0.1f;

    public JumpingCubeDecisionSubstate(IInitializer initializer, IJumpingCubeBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();

        float random = UnityEngine.Random.Range(0f, 1f);
        bool shouldJump = jumpChance > random;
        if(shouldJump) {
            IJumpingCubeBehaviour jumpingCubeBehaviour = (IJumpingCubeBehaviour)proxy;
            nextState = new EnemyJumpSubstate(initializer, jumpingCubeBehaviour);
        } else {
            nextState = new EnemyRecalculateForwardSubstate(initializer, proxy);
        }

    }

}
