﻿using TMI.Core;
using TMI.State;

public abstract class BasePlayerState : BaseStateWithProxy<IPlayerBehaviour> {

    protected BasePlayerState(IInitializer initializer, IPlayerBehaviour proxy) : base(initializer, proxy) {
    }

}
