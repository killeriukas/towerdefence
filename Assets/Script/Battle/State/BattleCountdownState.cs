﻿using System;
using TMI.Core;

public class BattleCountdownState : BaseBattleState {

    private static readonly TimeSpan countDownSpan = TimeSpan.FromSeconds(2);

    public BattleCountdownState(IInitializer initializer) : base(initializer) {
    }

    public override void Update() {
        base.Update();

        if(timeInState > countDownSpan) {
            nextState = new BattleFightState(initializer);
        }

    }
}
