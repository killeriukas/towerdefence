﻿using UnityEngine;

public class CameraBehaviour : BaseCameraBehaviour {


    public override void UpdateCameraControl(Transform root) {
        float yRotate = Input.GetAxis("Mouse X") * xSensitivity * Time.deltaTime;
        float xRotate = Input.GetAxis("Mouse Y") * ySensitivity * Time.deltaTime;

        root.Rotate(-xRotate, yRotate, 0f);

        Vector3 localRotation = root.localRotation.eulerAngles;


        // localRotation.x = Mathf.Clamp(localRotation.x, -45f, 45f);
        localRotation.z = 0f;
        root.localRotation = Quaternion.Euler(localRotation);
    }


}
