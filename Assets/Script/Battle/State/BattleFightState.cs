﻿using System;
using TMI.Core;
using TMI.Helper;
using TMI.State;

public class BattleFightState : BaseBattleState {

    private IStateMachine stateMachine;

    public BattleFightState(IInitializer initializer) : base(initializer) {
       
    }

    public override void Enter() {
        base.Enter();
        stateMachine = StateMachine.Create(new BattleSpawnEnemySubstate(initializer));

        Listen<PlayerDiedNotification>(this, OnPlayerDied);
        Listen<EnemyDiedNotification>(this, OnEnemyDied);
        Listen<GameTerminatedNotification>(this, OnGameTerminated);
    }

    private void OnGameTerminated(GameTerminatedNotification notification) {
        nextState = new BattleRestartState(initializer);
    }

    private void OnEnemyDied(EnemyDiedNotification notification) {
        if(notification.isBoss) {
            nextState = new BattleFinishState(initializer);
        }
    }

    private void OnPlayerDied(PlayerDiedNotification notification) {
        nextState = new BattleFinishState(initializer);
    }

    public override void Update() {
        base.Update();
        stateMachine.Update();
    }

    public override void Exit() {
        GeneralHelper.DisposeAndMakeDefault(ref stateMachine);
        base.Exit();
    }
}
