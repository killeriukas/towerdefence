﻿using UnityEngine;

public class JumpingCubeBehaviour : BaseEnemyBehaviour, IJumpingCubeBehaviour {

    [SerializeField]
    private Rigidbody _rigidBody;

    public Rigidbody mainRigidBody => _rigidBody;

    public override BaseEnemyDecisionSubstate CreateDecisionState() {
        return new JumpingCubeDecisionSubstate(initializer, this);
    }

}
