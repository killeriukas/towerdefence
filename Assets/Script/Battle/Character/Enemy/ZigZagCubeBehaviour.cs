﻿using TMI.Core;
using UnityEngine;

public class ZigZagCubeBehaviour : BaseEnemyBehaviour, IZigZagCubeBehaviour {

    public ZigZagCubeSettings settings { get; private set; } = new ZigZagCubeSettings();

    public override BaseEnemyDecisionSubstate CreateDecisionState() {
        return new ZigZagCubeDecisionSubstate(initializer, this);
    }

}
