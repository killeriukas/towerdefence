﻿using TMI.Core;

public class WeaponShootDelayState : BaseWeaponState {

    public WeaponShootDelayState(IInitializer initializer, IWeapon proxy) : base(initializer, proxy) {
    }

    public override void Update() {
        base.Update();

        if(timeInState > proxy.shotDelay) {
            nextState = new WeaponShootState(initializer, proxy);
        }
    }
}
