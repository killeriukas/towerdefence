﻿using TMI.Core;

public class PlayerSpawnState : BasePlayerState {

    public PlayerSpawnState(IInitializer initializer, IPlayerBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();

        proxy.mainCollider.enabled = false;

        nextState = new PlayerBattleState(initializer, proxy);

        Trigger(new PlayerSpawnedNotification());
    }


    public override void Exit() {
        proxy.mainCollider.enabled = true;
        base.Exit();
    }

}
