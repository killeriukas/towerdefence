﻿using TMI.Core;
using UnityEngine;

public class PlayerBattleState : BasePlayerState {

    public PlayerBattleState(IInitializer initializer, IPlayerBehaviour proxy) : base(initializer, proxy) {
    }

    public override void Enter() {
        base.Enter();
        Listen<PlayerDiedNotification>(this, OnPlayerDied);
    }

    private void OnPlayerDied(PlayerDiedNotification notification) {
        proxy.mainCollider.enabled = false;
        nextState = new PlayerFinishedState(initializer, proxy);
    }

    public override void Update() {
        base.Update();

        if(Input.GetMouseButtonDown(0)) {
            proxy.currentWeapon.StartShooting();
        }

        if(Input.GetMouseButtonUp(0)) {
            proxy.currentWeapon.EndShooting();
        }

    }

    public override void Exit() {
        proxy.currentWeapon.EndShooting();
        base.Exit();
    }

}
