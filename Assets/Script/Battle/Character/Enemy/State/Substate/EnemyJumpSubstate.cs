﻿using System;
using TMI.Core;
using UnityEngine;

public class EnemyJumpSubstate : BaseEnemySubstate {

    private static readonly TimeSpan jumpDuration = TimeSpan.FromSeconds(2);

    private BattleManager battleManager;

    public EnemyJumpSubstate(IInitializer initializer, IJumpingCubeBehaviour proxy) : base(initializer, proxy) {
        this.battleManager = initializer.GetManager<BattleManager>();
    }

    public override void Enter() {
        base.Enter();

        IJumpingCubeBehaviour jumpingCubeBehaviour = (IJumpingCubeBehaviour)proxy;
        Rigidbody rigidBody = jumpingCubeBehaviour.mainRigidBody;

        EnablePhysics(true);
        rigidBody.AddForce(Vector3.up * 5f, ForceMode.Impulse);
    }

    public override void Update() {
        base.Update();

        if(timeInState > jumpDuration) {
            IJumpingCubeBehaviour jumpingCubeBehaviour = (IJumpingCubeBehaviour)proxy;
            nextState = new JumpingCubeDecisionSubstate(initializer, jumpingCubeBehaviour);
        }
    }

    private void EnablePhysics(bool enable) {
        IJumpingCubeBehaviour jumpingCubeBehaviour = (IJumpingCubeBehaviour)proxy;
        Rigidbody rigidBody = jumpingCubeBehaviour.mainRigidBody;
        rigidBody.isKinematic = !enable;
        jumpingCubeBehaviour.mainCollider.isTrigger = !enable;
        rigidBody.useGravity = enable;
    }


    public override void Exit() {
        EnablePhysics(false);
        base.Exit();
    }
}
