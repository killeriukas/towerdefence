﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "Template/Game Data")]
public class GameData : ScriptableObject {

    public int mapSize;

    [SerializeField]
    private float spawnFrequenceInSeconds;

    public TimeSpan spawnFrequenceDuration => TimeSpan.FromSeconds(spawnFrequenceInSeconds);

    public int cubesAmountTillTitanAppears;

    public int maxCubesOnScreen;
}
