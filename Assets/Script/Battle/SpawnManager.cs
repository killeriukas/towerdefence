﻿using System.Collections.Generic;
using TMI.Core;
using TMI.Helper;
using UnityEngine;

public class SpawnManager : BaseNotificationManager {

    private List<GameObjectPool<BaseEnemyBehaviour>> normalCubeTypeList = new List<GameObjectPool<BaseEnemyBehaviour>>();

    private GameData gameData;

    private GameObjectPool<BaseEnemyBehaviour> simpleCubeEnemyPool;
    private GameObjectPool<BaseEnemyBehaviour> jumpingCubeEnemyPool;
    private GameObjectPool<BaseEnemyBehaviour> zigZagCubeEnemyPool;

    private GameObjectPool<BaseEnemyBehaviour> titanCubeEnemyPool;

    private int totalCubesSpawned = 0;

    private int currentlySpawnedCubeAmount = 0;

    public bool canSpawnAnotherCube => !isTitanSpawned && currentlySpawnedCubeAmount < gameData.maxCubesOnScreen;
    private bool isTitanSpawned = false;

    public override void Setup(IInitializer initializer, bool isNew) {
        base.Setup(initializer, isNew);

        Listen<EnemyDiedNotification>(this, OnEnemyDied);
    }

    private void OnEnemyDied(EnemyDiedNotification notification) {
        --currentlySpawnedCubeAmount;
    }

    public void Initialize(SpawnAccessor spawnAccessor, int startingAmount, GameData gameData) {
        this.gameData = gameData;

        simpleCubeEnemyPool = new GameObjectPool<BaseEnemyBehaviour>(initializer, spawnAccessor.simpleCubeEnemyPrefab, startingAmount);
        jumpingCubeEnemyPool = new GameObjectPool<BaseEnemyBehaviour>(initializer, spawnAccessor.jumpingCubeEnemyPrefab, startingAmount);
        zigZagCubeEnemyPool = new GameObjectPool<BaseEnemyBehaviour>(initializer, spawnAccessor.zigZagCubeEnemyPrefab, startingAmount);

        normalCubeTypeList.Add(simpleCubeEnemyPool);
        normalCubeTypeList.Add(jumpingCubeEnemyPool);
        normalCubeTypeList.Add(zigZagCubeEnemyPool);

        titanCubeEnemyPool = new GameObjectPool<BaseEnemyBehaviour>(initializer, spawnAccessor.titanCubeEnemyPrefab, 2);
    }

    public void SpawnRandomEnemy(int mapSize) {
        Vector2 randomPosition = Random.insideUnitCircle.normalized * mapSize;

        //spawn titan
        if(totalCubesSpawned == gameData.cubesAmountTillTitanAppears) {
            BaseEnemyBehaviour titanEnemyBehaviour = titanCubeEnemyPool.Get();

            Vector3 enemyPosition = new Vector3(randomPosition.x, titanEnemyBehaviour.modelScale / 2, randomPosition.y);

            titanEnemyBehaviour.Initialize(titanCubeEnemyPool, enemyPosition);

            isTitanSpawned = true;
        } else {
            int randomCubeIndex = Random.Range(0, normalCubeTypeList.Count);

            GameObjectPool<BaseEnemyBehaviour> randomEnemyType = normalCubeTypeList[randomCubeIndex];
            BaseEnemyBehaviour enemyBehaviour = randomEnemyType.Get();

            Vector3 enemyPosition = new Vector3(randomPosition.x, enemyBehaviour.modelScale / 2, randomPosition.y);

            enemyBehaviour.Initialize(randomEnemyType, enemyPosition);

            ++totalCubesSpawned;
        }

        ++currentlySpawnedCubeAmount;
    }

    protected override void OnDestroy() {
        normalCubeTypeList.Clear();
        GeneralHelper.DisposeAndMakeDefault(ref simpleCubeEnemyPool);
        GeneralHelper.DisposeAndMakeDefault(ref titanCubeEnemyPool);
        GeneralHelper.DisposeAndMakeDefault(ref jumpingCubeEnemyPool);
        GeneralHelper.DisposeAndMakeDefault(ref zigZagCubeEnemyPool);
        base.OnDestroy();
    }

}