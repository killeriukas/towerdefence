﻿using UnityEngine;

public interface IJumpingCubeBehaviour : IEnemyBehaviour {
    Rigidbody mainRigidBody { get; }
}
